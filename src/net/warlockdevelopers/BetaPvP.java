package net.warlockdevelopers;

import net.warlockdevelopers.managers.ListenerManager;

import org.bukkit.plugin.java.JavaPlugin;

public class BetaPvP extends JavaPlugin {
	
	private static BetaPvP instance;
	private ListenerManager listenerManager;
	
	@Override
	public void onEnable() {
		instance = this;
		
		listenerManager = new ListenerManager(getServer().getPluginManager());
		listenerManager.register();
	}
	
	@Override
	public void onDisable() {
		instance = null;
	}
	
	public static BetaPvP getInstance() {
		return instance;
	}

}
