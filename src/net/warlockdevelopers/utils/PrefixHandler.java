package net.warlockdevelopers.utils;

import org.bukkit.ChatColor;

public class PrefixHandler {
	
	public static String MAIN_PREFIX = ChatColor.BLUE + "BetaPvP> " + ChatColor.GRAY;
	public static String JOIN_PREFIX = ChatColor.DARK_GRAY + "Join> " + ChatColor.GRAY;
	public static String QUIT_PREFIX = ChatColor.DARK_GRAY + "Quit> " + ChatColor.GRAY;
	public static String SERVER_PREFIX = ChatColor.RED + "Server> " + ChatColor.GRAY;
	
	public static String getMainPrefix() {
		return MAIN_PREFIX;
	}
	
	public static String getJoinPrefix() {
		return JOIN_PREFIX;
	}
	
	public static String getQuitPrefix() {
		return QUIT_PREFIX;
	}
	
	public static String getServerPrefix() {
		return SERVER_PREFIX;
	}

}
