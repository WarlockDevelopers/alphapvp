package net.warlockdevelopers.listeners;

import net.warlockdevelopers.utils.PrefixHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {
	
	String server = PrefixHandler.getServerPrefix();
	
	@EventHandler
	public void onPlayerBreakBlock(BlockBreakEvent event) {
		Player player = event.getPlayer();
		event.setCancelled(true);
		player.sendMessage(server + "Sorry, but you can't break blocks.");
		
		if(player.hasPermission("betapvp.block.bypass")) {
			event.setCancelled(false);
		}
	}

}
