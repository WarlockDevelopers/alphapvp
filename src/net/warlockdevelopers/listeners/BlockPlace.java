package net.warlockdevelopers.listeners;

import net.warlockdevelopers.utils.PrefixHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlace implements Listener {
	
	String server = PrefixHandler.getServerPrefix();
	
	@EventHandler
	public void onPlayerBreakBlock(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		event.setCancelled(true);
		player.sendMessage(server + "Sorry, but you can't place blocks.");
		
		if(player.hasPermission("betapvp.block.bypass")) {
			event.setCancelled(false);
		}
	}

}
