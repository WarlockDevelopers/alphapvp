package net.warlockdevelopers.listeners;

import net.warlockdevelopers.BetaPvP;
import net.warlockdevelopers.utils.PrefixHandler;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {
	
	String join = PrefixHandler.getJoinPrefix();
	String server = PrefixHandler.getServerPrefix();
	
	BetaPvP plugin = BetaPvP.getInstance();
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		player.getInventory().clear();
		event.setJoinMessage(join + player.getName());
		player.sendMessage(server + "This server is running BetaPvP v" + plugin.getDescription().getVersion());
	}

}
