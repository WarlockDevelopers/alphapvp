package net.warlockdevelopers.managers;

import net.warlockdevelopers.BetaPvP;
import net.warlockdevelopers.listeners.PlayerJoin;

import org.bukkit.plugin.PluginManager;

public class ListenerManager {
	
private PluginManager pluginManager;
	
	public ListenerManager(PluginManager pluginManager) {
		this.pluginManager = pluginManager;
	}
	
	public void register() {
		this.pluginManager.registerEvents(new PlayerJoin(), BetaPvP.getInstance());
	}

}
